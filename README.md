# Anonymizer

Library for anonymization of data

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `anonymizer` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:anonymizer, git: "https://gitlab.com/saneery/anonymizer.git"}
  ]
end
```

## Examples

### Map

Supports only maps with atom keys

```elixir
iex> data = %{
    customer: %{
      name: "Andrew",
      surname: "Yudin",
      email: "andrew@channex.io"
    },
    checkin_date: "2019-02-20",
    checkout_date: "2019-02-21"
  }
iex> profile = %{
    kind: Anonymizer.Structure.Map,
    selectors: [
      {"customer/name", :mask},
      {"customer/surname", :mask},
      {"customer/email", :drop}
    ]
  }
iex> Anonymizer.anonymize(data, profile)
{:ok,
 %{
   checkin_date: "2019-02-20",
   checkout_date: "2019-02-21",
   customer: %{name: "A**%%w", surname: "Y&@%n"}
 }}
```

### JSON

```elixir
iex> data = "{\"checkin_date\":\"2019-02-20\",\"checkout_date\":\"2019-02-21\",\"customer\":{\"email\":\"andrew@channex.io\",\"name\":\"Andrew\",\"surname\":\"Yudin\"}}"
iex> profile = %{
    kind: Anonymizer.Structure.JSON,
    selectors: [
      {"customer/name", :mask},
      {"customer/surname", :mask},
      {"customer/email", :drop}
    ]
  }
iex> Anonymizer.anonymize(data, profile)
{:ok,
 "{\"checkin_date\":\"2019-02-20\",\"checkout_date\":\"2019-02-21\",\"customer\":{\"name\":\"A%$!%w\",\"surname\":\"Y$&@n\"}}"}
```

## Extendability

To add new logic to anonymize different data, you should implement your own module for that. The module should have two required function `decode/1` and `encode/1` to prepaire data for easy work. Also it should have anonymization functions e.g. `mask/2` and `drop/2`

### Example of an extension module

```elixir
defmodule Anonymizer.JSON do
  @behaviour Anonymizer.Behaviour

  def decode(data), do: Jason.decode(data, keys: :atoms)

  def encode(data), do: Jason.encode(data)

  def mask(key, decoded_data) do
    Map.put(decoded_data, key, "***")
  end
end

iex> data = "{\"name\": \"John\", \"surname\": \"Smith\"}"
iex> profile = %{kind: Anonymizer.JSON, selectors: [{"name", :mask}]}
iex> Anonymizer.anonymize(data, profile)
{:ok, "{\"name\":\"John\",\"surname\":\"Smith\",\"name\":\"***\"}"}
```
