defmodule Anonymizer do
  @moduledoc """
  Anonymizer of data
  """

  @spec anonymize(data :: map, profile :: map) :: map
  def anonymize(data, profile) do
    profile
    |> validate_profile()
    |> decode_data(data)
    |> apply_anonymize(profile)
    |> encode_data(profile)
  end

  defp validate_profile(%{kind: kind, selectors: selector}) when is_list(selector) do
    with {:module, ^kind} <- Code.ensure_compiled(kind),
         true <- has_behaviour?(kind) do
      kind
    else
      _ ->
        {:error, :invalid_module}
    end
  end

  defp validate_profile(_), do: {:error, :invalid_profile}

  defp decode_data({:error, _} = err, _), do: err

  defp decode_data(module, data) do
    case module.decode(data) do
      {:ok, decoded_data} ->
        decoded_data

      error ->
        error
    end
  end

  defp apply_anonymize({:error, _} = err, _), do: err

  defp apply_anonymize(data, %{kind: module, selectors: selectors}) do
    Enum.reduce_while(selectors, data, fn {path, func}, acc ->
      case run_command(module, func, [path, acc]) do
        {:error, _} = err ->
          {:halt, err}

        updated_acc ->
          {:cont, updated_acc}
      end
    end)
  end

  defp encode_data({:error, _} = err, _), do: err

  defp encode_data(data, %{kind: module}) do
    case module.encode(data) do
      {:ok, encoded_data} ->
        {:ok, encoded_data}

      error ->
        error
    end
  end

  defp has_behaviour?(module) do
    behaviours =
      :attributes
      |> module.module_info()
      |> Keyword.get(:behaviour, [])

    Anonymizer.Behaviour in behaviours
  end

  defp run_command(module, function, attr) do
    if function_exported?(module, function, length(attr)) do
      apply(module, function, attr)
    else
      {:error, :no_function}
    end
  end
end
