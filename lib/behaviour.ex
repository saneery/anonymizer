defmodule Anonymizer.Behaviour do
  @callback decode(data :: term) :: {:ok, data :: term} | {:error, reason :: atom}
  @callback encode(data :: term) :: {:ok, data :: term} | {:error, reason :: atom}
end
