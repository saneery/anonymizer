defmodule Anonymizer.Helper do
  @mask_chars ~c(!@$%&*)

  def apply_mask(string) when is_binary(string) do
    [first_char | tail] =
      string
      |> String.to_charlist()

    tail
    |> generate_mask([first_char])
    |> List.to_string()
  end

  def apply_mask(val), do: val

  defp generate_mask([last_char], acc) do
    Enum.reverse([last_char | acc])
  end

  defp generate_mask([_char | tail], acc) do
    new_acc = [Enum.random(@mask_chars) | acc]
    generate_mask(tail, new_acc)
  end

  def path_to_list(path) do
    path
    |> String.split("/")
    |> Enum.map(&String.to_atom/1)
  end
end
