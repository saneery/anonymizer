defmodule Anonymizer.Structure.JSON do
  @behaviour Anonymizer.Behaviour

  alias Anonymizer.Structure.Map

  def decode(data), do: Jason.decode(data, keys: :atoms)

  def encode(data), do: Jason.encode(data)

  defdelegate mask(path, map), to: Map

  defdelegate drop(path, map), to: Map
end
