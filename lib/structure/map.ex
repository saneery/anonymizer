defmodule Anonymizer.Structure.Map do
  @behaviour Anonymizer.Behaviour

  alias Anonymizer.Helper

  def decode(map), do: {:ok, map}

  def encode(map), do: {:ok, map}

  def drop(path, map) do
    path_list = Helper.path_to_list(path)
    {_, updated_map} = pop_in(map, path_list)
    updated_map
  end

  def mask(path, map) do
    path_list = Helper.path_to_list(path)

    case get_in(map, path_list) do
      nil ->
        map

      val ->
        put_in(map, path_list, Helper.apply_mask(val))
    end
  end
end
