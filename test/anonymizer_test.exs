defmodule AnonymizerTest do
  use ExUnit.Case
  doctest Anonymizer

  describe "Map" do
    test "mask and drop" do
      data = %{
        customer: %{
          name: "Andrew",
          surname: "Yudin",
          email: "andrew@channex.io"
        },
        checkin_date: "2019-02-20",
        checkout_date: "2019-02-21"
      }

      profile = %{
        kind: Anonymizer.Structure.Map,
        selectors: [
          {"customer/name", :mask},
          {"customer/surname", :mask},
          {"customer/email", :drop}
        ]
      }

      assert {:ok, updated_data} = Anonymizer.anonymize(data, profile)
      assert updated_data[:customer][:email] == nil
      assert data[:customer][:name] != updated_data[:customer][:name]
      assert data[:customer][:surname] != updated_data[:customer][:surname]
      assert data[:customer][:checkin_date] == updated_data[:customer][:checkin_date]
    end

    test "invalid module" do
      data = %{
        customer: %{
          name: "Andrew"
        },
        checkin_date: "2019-02-20",
        checkout_date: "2019-02-21"
      }

      profile = %{
        kind: Map,
        selectors: [
          {"customer/name", :mask}
        ]
      }

      assert {:error, :invalid_module} = Anonymizer.anonymize(data, profile)
    end
  end

  describe "JSON" do
    test "success" do
      data = Jason.encode!(%{
        customer: %{
          name: "Andrew",
          surname: "Yudin",
          email: "andrew@channex.io"
        },
        checkin_date: "2019-02-20",
        checkout_date: "2019-02-21"
      })

      profile = %{
        kind: Anonymizer.Structure.JSON,
        selectors: [
          {"customer/name", :mask},
          {"customer/surname", :mask},
          {"customer/email", :drop}
        ]
      }

      assert {:ok, updated_json} = Anonymizer.anonymize(data, profile)
    end
  end
end
